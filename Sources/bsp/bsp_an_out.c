/**
 * ************************************************************************************************
 * @file    bsp_an_out.c
 *
 * @date    18 Feb 2017
 * @author  Ivan / CTMLAB LLC
 * @version 1.0
 * @brief   Файл содержит функции для генерации ШИМ на выводах микроконтроллера.
 * @note    Задайте следующие значения:
 *          (BSP_PWM_DUTY_CYCLE_16B_MAX) , (BSP_PWM_AN_OUT_MAX_VOLTAGE_MV)
 *          (BSP_PWM_MCU_OUT_MAX_MV) , (BSP_PWM_MCU_POWER_MV).
 * ************************************************************************************************
 */

// --------------------------------------------------------- Includes ----------------------------:
#include "bsp/bsp_an_out.h"

#include <stdint.h>
#include <stdbool.h>
#include "PWM1.h"
// --------------------------------------------------------- Defines -----------------------------:
#define BSP_PWM_DUTY_CYCLE_16B_MAX      (0xFFFF)        // максимальное значение скважности в 2-х байтном виде
#define BSP_PWM_AN_OUT_MAX_VOLTAGE_MV   (12000)         // максимальное значение напряжения на аналоговом выходе
#define BSP_PWM_MCU_OUT_MAX_MV          (4750)          // значение на выходе MCU, соответствующее BSP_PWM_AN_OUT_MAX_VOLTAGE_MV
#define BSP_PWM_MCU_POWER_MV            (5000)          // напряжение питания

// Максимальная используемая скважность ШИМ для данной платы:
#define BSP_PWM_DUTY_CYCLE_12B_CURRENT  (uint32_t)(BSP_PWM_MCU_OUT_MAX_MV * BSP_PWM_DUTY_CYCLE_16B_MAX / BSP_PWM_MCU_POWER_MV)
// --------------------------------------------------------- Global variables --------------------:

// --------------------------------------------------------- Private variables -------------------:
LDD_TDeviceData*     pwm1_device_ptr;   // Указатель на структуру ШИМ генератора, создаваемую Processor Expert.

// --------------------------------------------------------- Private function prototypes ---------:

// --------------------------------------------------------- Global functions --------------------:
/************************************
 * @brief   Инициализирует аналоговый выход.
 * @param[in]   None.
 * @return      None
 ************************************/
void bsp_an_out_init (void)
{
    pwm1_device_ptr = PWM1_Init(NULL);
}



/************************************
 * @brief   Деинициализирует аналоговый выход.
 * @param[in]   None.
 * @return      None
 ************************************/
void bsp_an_out_deinit (void)
{
    (void)PWM1_Deinit(pwm1_device_ptr);
}



/************************************
 * @brief       Устанавливает новое значени напряжения на аналоговом выходе.
 * @param[in]   voltage - значение напряжения в миливольтах.
 * @return       0  - успешное выполнение;
 *              -1  - ошибка.
 ************************************/
int bsp_an_out_set_voltage(uint32_t voltage)           // todo: без флоата
{
    // return, если недопустимое значение напряжения:
    if (voltage > BSP_PWM_AN_OUT_MAX_VOLTAGE_MV)
        return -1;

    uint32_t temp_value = 0xFFFF - (voltage * BSP_PWM_DUTY_CYCLE_12B_CURRENT) / BSP_PWM_AN_OUT_MAX_VOLTAGE_MV ;
    PWM1_SetRatio16 (pwm1_device_ptr, (uint16_t)temp_value);    // Устанавливаем новое значение скважности ШИМ,
                               // в данном случае скважность отвечает за длительность низкого уровня на выходе.
    return 0;
}



/************************************
 * @brief   Включение вывода напряжения на аналоговый выход.
 * @param[in]   None.
 * @return      None
 ************************************/
void bsp_an_out_enable (void)
{
    (void)PWM1_Enable(pwm1_device_ptr);
}

/************************************
 * @brief   Отключение вывода напряжения на аналоговый выход.
 * @param[in]   None.
 * @return      None
 ************************************/
void bsp_an_out_disable (void)
{
    (void)PWM1_Disable(pwm1_device_ptr);
}


// --------------------------------------------------------- Private functions -------------------:

// --------------------------------------------------------- Private constants -------------------:

/**********************************************************************************END OF FILE****/
