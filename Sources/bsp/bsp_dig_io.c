/**
 * ************************************************************************************************
 * @file    bsp_dig_io.c
 *
 * @date    26 Feb 2017
 * @author  Ivan / CTMLAB LLC
 * @version 1.0
 * @brief   Файл содержит функции для работы с дискретными цифровыми выходами/входами.
 * ************************************************************************************************
 */

// --------------------------------------------------------- Includes ----------------------------:
#include "bsp/bsp_dig_io.h"

#include "MKE06Z4.h"
#include "GPIO_PDD.h"
// --------------------------------------------------------- Defines -----------------------------:
// Порт дискретных входов:
#define DIG_INPUT_PORT              GPIOA_BASE_PTR
// Порт дискретных выходов:
#define DIG_OUTPUT_PORT             GPIOA_BASE_PTR

// --------------------------------------------------------- Types -------------------------------:

// --------------------------------------------------------- Global variables --------------------:

// --------------------------------------------------------- Private variables -------------------:


// --------------------------------------------------------- Private function prototypes ---------:

// --------------------------------------------------------- Global functions --------------------:

/************************************
 * @brief   Инициализация всех дискретных цифровых входов/выходов.
 * @param[in]       None.
 * @return          None.
 ************************************/
void bsp_dig_io_init (void)
{
    // Конфигурирование цифровых входов ========:
    GPIO_PDD_SetPortInputDirectionMask(DIG_INPUT_PORT, BSP_DIG_IN_CD_INVON);
    GPIO_PDD_SetPortInputDirectionMask(DIG_INPUT_PORT, BSP_DIG_IN_CD_OVRTM);
    GPIO_PDD_SetPortInputDirectionMask(DIG_INPUT_PORT, BSP_DIG_IN_CD_SHTCT);
    GPIO_PDD_SetPortInputDirectionMask(DIG_INPUT_PORT, BSP_DIG_IN_CD_INVRDY);
    GPIO_PDD_SetPortInputDirectionMask(DIG_INPUT_PORT, BSP_DIG_IN_CD_FLTD);
    GPIO_PDD_SetPortInputDirectionMask(DIG_INPUT_PORT, BSP_DIG_IN_CD_HVRDY);

    // Выводы после инициализации не должны быть активны!!! + задавать значения выводов лучше до перевода их в состояние выхода
    GPIO_PDD_ClearPortDataOutputMask(DIG_OUTPUT_PORT, BSP_DIG_OUT_CD_ON);
    GPIO_PDD_ClearPortDataOutputMask(DIG_OUTPUT_PORT, BSP_DIG_OUT_CD_1KW);
    GPIO_PDD_ClearPortDataOutputMask(DIG_OUTPUT_PORT, BSP_DIG_OUT_CD_2KW);

    // Конфигурирование цифровых выходов ========:
    GPIO_PDD_SetPortOutputDirectionMask(DIG_OUTPUT_PORT, BSP_DIG_OUT_CD_ON);
    GPIO_PDD_SetPortOutputDirectionMask(DIG_OUTPUT_PORT, BSP_DIG_OUT_CD_1KW);
    GPIO_PDD_SetPortOutputDirectionMask(DIG_OUTPUT_PORT, BSP_DIG_OUT_CD_2KW);
}

/************************************
 * @brief       Получить состояние цифрового входа pin
 * @param[in]   pin .
 * @return      None.
 ************************************/
uint32_t bsp_dig_in_get (void)
{
    return GPIO_PDD_GetPortDataInput(DIG_INPUT_PORT);
}

/************************************
 * @brief   Установить новые значения цифровых выходов в единицу
 * @param[in]   pin_mask    - маска пинов:
 *                              BSP_DIG_OUT_CD_ON;
 *                              BSP_DIG_OUT_CD_1KW;
 *                              BSP_DIG_OUT_CD_2KW.
 * @return      None.
 ************************************/
void bsp_dig_out_set (uint32_t pin_mask)
{
    GPIO_PDD_SetPortDataOutputMask(DIG_OUTPUT_PORT, pin_mask);
}

/************************************
 * @brief   Установить новые значения цифровых выходов в ноль
 * @param[in]   pin_mask    - маска пинов:
 *                              BSP_DIG_OUT_CD_ON;
 *                              BSP_DIG_OUT_CD_1KW;
 *                              BSP_DIG_OUT_CD_2KW.
 * @return      None.
 ************************************/
void bsp_dig_out_clear (uint32_t pin_mask)
{
    GPIO_PDD_ClearPortDataOutputMask(DIG_OUTPUT_PORT, pin_mask);
}

// --------------------------------------------------------- Private functions -------------------:

// --------------------------------------------------------- Private constants -------------------:

/**********************************************************************************END OF FILE****/
