/**
 * ************************************************************************************************
 * @file    bsp_dig_io.h
 *
 * @date    26 Feb 2017
 * @author  Ivan / CTMLAB LLC
 * @version 1.1
 * @brief   Header for bsp_dig_io.c.
 * ************************************************************************************************
 */
#ifndef BSP_BSP_DIG_IO_H_
#define BSP_BSP_DIG_IO_H_
// -------------------------------------------------------- Includes -----------------------------:
#include <stdint.h>
#include <stdbool.h>

// -------------------------------------------------------- Exported define ----------------------:

/*
 *  @brief Маски дискретных входов:
 */
#define BSP_DIG_IN_CD_INVON      GPIO_PDD_PIN_16
#define BSP_DIG_IN_CD_OVRTM      GPIO_PDD_PIN_17
#define BSP_DIG_IN_CD_SHTCT      GPIO_PDD_PIN_18
#define BSP_DIG_IN_CD_INVRDY     GPIO_PDD_PIN_19
#define BSP_DIG_IN_CD_FLTD       GPIO_PDD_PIN_12
#define BSP_DIG_IN_CD_HVRDY      GPIO_PDD_PIN_13

/*
 *  @brief Маски дискретных выходов:
 */
#define BSP_DIG_OUT_CD_ON          GPIO_PDD_PIN_10
#define BSP_DIG_OUT_CD_1KW         GPIO_PDD_PIN_9
#define BSP_DIG_OUT_CD_2KW         GPIO_PDD_PIN_8

// -------------------------------------------------------- Exported types -----------------------:

// -------------------------------------------------------- Private macros -----------------------:

// -------------------------------------------------------- Exported variables -------------------:

// -------------------------------------------------------- Exported functions -------------------:
void bsp_dig_io_init (void);
uint32_t bsp_dig_in_get (void);
void bsp_dig_out_set (uint32_t pin_mask);
void bsp_dig_out_clear (uint32_t pin_mask);


// -------------------------------------------------------- Exported constants -------------------:


#endif /* BSP_BSP_DIG_IO_H_ */
