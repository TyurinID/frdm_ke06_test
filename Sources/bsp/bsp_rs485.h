/**
 * ************************************************************************************************
 * @file    bsp_rs485.h
 *
 * @date    20 Feb 2017
 * @author  Ivan / CTMLAB LLC
 * @version 1.2
 * @brief
 * ************************************************************************************************
 */
#ifndef BSP_BSP_RS485_H_
#define BSP_BSP_RS485_H_

// -------------------------------------------------------- Includes -----------------------------:
#include <stdint.h>
#include <stdbool.h>
#include "MKE06Z4.h"
// -------------------------------------------------------- Exported define ----------------------:

// -------------------------------------------------------- Exported types -----------------------:
/*
 * Возвращаемые значения функции bsp_rs485_listen:
 */
typedef enum  {
    BSP_RS485_FUNC_BUSY,
    BSP_RS485_FUNC_READY,
    BSP_RS485_FUNC_ERR
}   bsp_rs485_funct_ret_t;
// -------------------------------------------------------- Private macros -----------------------:

// -------------------------------------------------------- Exported variables -------------------:
extern volatile bool bsp_rs485_sys_tim_flg;             // Флаг, выставляемый по прерыванию от таймера SYS_TIM
// -------------------------------------------------------- Exported functions -------------------:
int                      bsp_rs485_init (void);
bsp_rs485_funct_ret_t    bsp_rs485_listen (uint8_t* msg_received, uint32_t* msg_received_size);
bsp_rs485_funct_ret_t    bsp_rs485_answer (uint8_t* tx_msg_buff, uint32_t tx_msg_size);
void                     bsp_rs485_byte_received_callback (void);
void                     bsp_rs485_msg_transmit_callback (void);
void                     bsp_rs485_sys_tim_callback (void);
// -------------------------------------------------------- Exported constants -------------------:


#endif /* BSP_BSP_RS485_H_ */
