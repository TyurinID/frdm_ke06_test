/**
 * ************************************************************************************************
 * @file    bsp_rs485.c
 *
 * @date    20 Feb 2017
 * @author  Ivan / CTMLAB LLC
 * @version 1.2
 * @brief   Блок взаимодействия по RS485.
 *          Определите положение пина DE (BSP_RS485_PIN_DE_BASE) и (BSP_RS485_PIN_DE_MASK).
 * ************************************************************************************************
 */

// --------------------------------------------------------- Includes ----------------------------:
#include    "bsp/bsp_rs485.h"

#include    "GPIO_PDD.h"
#include    "UART_PDD.h"
#include    "UART1.h"
#include    "csp/csp_sys_tim.h"

// --------------------------------------------------------- Defines -----------------------------:
/*
 * Параметры пина DE:
 */
#define BSP_RS485_PIN_DE_BASE       GPIOA_BASE_PTR
#define BSP_RS485_PIN_DE_MASK       GPIO_PDD_PIN_21     // PTC5

#define MSG_SILENCE_TIME_TICKS      34000   // Количество тиков, загружаемое в SYS_TIM для выдержки
                                            // паузы в 2мс: (62,5 нс * 32000) = 2 мс.

// --------------------------------------------------------- Types -------------------------------:
/*
 * Этапы приема сообщения:
 */
typedef enum  {
    BSP_RS485_LISTEN_START,
    BSP_RS485_LISTEN_WAIT
}   rs485_listen_stg_t;
/*
 * Этапы отправления сообщения:
 */
typedef enum  {
    BSP_RS485_ANSWER_START,
    BSP_RS485_ANSWER_WAIT
}   rs485_answer_stg_t;

/*
 * @brief Структура для хранения параметров модуля RS485:
 */
typedef struct  {
    uint8_t*                    rx_buff;            // Указатель на буффер для хранения входящего сообщения;
    uint32_t                    rx_buff_cnt;        // Счетчик принятых байтов;
    rs485_listen_stg_t          listen_stg;         // Этап работы функции приема сообщения;
//	bool						msg_rx_received;    // Флаг окончания приема сообщения;
    rs485_answer_stg_t          answer_stg;         // Этап работы функции отправки сообщения;
//    bool                        answer_sended_flg;  // Флаг окончания отправки сообщения.
}   bsp_rs485_t;

// --------------------------------------------------------- Macros ------------------------------:




// --------------------------------------------------------- Global variables --------------------:
volatile bool bsp_rs485_sys_tim_flg;             // Флаг, выставляемы по прерыванию от таймера SYS_TIM

// --------------------------------------------------------- Private variables -------------------:
static bsp_rs485_t          bsp_rs485;      // Структура для хранения параметров модуля RS485.
static LDD_TDeviceData*     LDD_Uart1;      // Структура для CSP.
// --------------------------------------------------------- Private function prototypes ---------:

// --------------------------------------------------------- Global functions --------------------:


/************************************
 * Инициализирует модуль RS485.
 * param[in] None.
 * @return   0 - успешная инициализация;
 *          -1 - ошибка инициализации
 ************************************/
int  bsp_rs485_init (void)
{
    // Инициализация структуры значениями по умолчанию =========:
    bsp_rs485.answer_stg            = BSP_RS485_ANSWER_START;
    bsp_rs485.listen_stg            = BSP_RS485_LISTEN_START;
    bsp_rs485.rx_buff               = NULL;
    bsp_rs485.rx_buff_cnt           = 0;

    // Инициализация пина DE ===================================:
    GPIO_PDD_SetPortOutputDirectionMask(BSP_RS485_PIN_DE_BASE, BSP_RS485_PIN_DE_MASK);
    GPIO_PDD_ClearPortDataOutputMask(BSP_RS485_PIN_DE_BASE, BSP_RS485_PIN_DE_MASK);     // Прием.

    // Инициализация UART ======================================:
    LDD_Uart1 = UART1_Init(NULL);

    return 0;
}

/************************************
 * Конфигурируем RS485 на прием. Ждем сообщения.
 * Считываем пришедшее сообщение в rx_buff через прерывания.
 * Вызываем функцию по адресу appcall_func.
 * param[in] None.
 * @return  Состояние функции:
 *          BSP_RS485_FUNC_BUSY   - ожидание/прием сообщения;
 *          BSP_RS485_FUNC_READY  - сообщение получено;
 *          BSP_RS485_FUNC_ERR    - ошибка выполнения;
 * @note:  Отправление и прием сообщений одновременно не возможны!
 ************************************/
bsp_rs485_funct_ret_t bsp_rs485_listen (uint8_t* rx_msg_buff, uint32_t* rx_msg_size)
{
    static bsp_rs485_funct_ret_t ret = BSP_RS485_FUNC_BUSY;
    switch(bsp_rs485.listen_stg)    {

    case BSP_RS485_LISTEN_START:
        // Обнуляем счетчик буффера приема и запускаем получение первого байта:
        bsp_rs485.rx_buff       = rx_msg_buff;
        bsp_rs485.rx_buff_cnt   = 0;
        UART1_ReceiveBlock(LDD_Uart1, bsp_rs485.rx_buff, 1);
        bsp_rs485.listen_stg    = BSP_RS485_LISTEN_WAIT;
        ret                     = BSP_RS485_FUNC_BUSY;
        break;

    case BSP_RS485_LISTEN_WAIT:
        // Ждем выставление флага окончания периода "тишины" после сообщения:
        if (bsp_rs485_sys_tim_flg)  {
            bsp_rs485_sys_tim_flg   = false;
			// Отменить дальнейший прием байтов:
            UART1_CancelBlockReception(LDD_Uart1);
            *rx_msg_size            = bsp_rs485.rx_buff_cnt;
            // appcall_func(bsp_rs485.rx_buff, bsp_rs485.rx_buff_cnt);
            bsp_rs485.rx_buff_cnt   = 0;
            bsp_rs485.listen_stg    = BSP_RS485_LISTEN_START;
            ret                     = BSP_RS485_FUNC_READY;
        }
        break;

    default:
        ret                     = BSP_RS485_FUNC_ERR;
        break;
    }
    return ret;
}


/************************************
 * @brief   Конфигурируем RS485 посылку сообщения.
 *          Ждем окончания отправки сообшения по прерваниям.
 * @param[in] None.
 * @return  Состояние функции:
 *          BSP_RS485_FUNC_BUSY   - ожидание/прием сообщения;
 *          BSP_RS485_FUNC_READY  - сообщение получено;
 *          BSP_RS485_FUNC_ERR    - ошибка выполнения;
 * @note:  Отправление и прием сообщений одновременно не возможны!
 ************************************/
bsp_rs485_funct_ret_t bsp_rs485_answer (uint8_t* tx_msg_buff, uint32_t tx_msg_size)
{
    static bsp_rs485_funct_ret_t ret = BSP_RS485_FUNC_BUSY;
    switch(bsp_rs485.answer_stg)    {

    case BSP_RS485_ANSWER_START:
        // Отключаем Rx ========================:
        UART1_TurnRxOff(LDD_Uart1);
        // Переключаем пин DE на отправку ==========================:
        GPIO_PDD_SetPortDataOutputMask(BSP_RS485_PIN_DE_BASE, BSP_RS485_PIN_DE_MASK);
        // Задержка в: 62,5 нс * 24 = 1.5 мкс ====:
        SYS_TIM_START(24);
        while(bsp_rs485_sys_tim_flg == 0)   {};
        bsp_rs485_sys_tim_flg = false;
        // Запуск посылки по прерываниям ==========================:
        UART1_SendBlock(LDD_Uart1, tx_msg_buff, tx_msg_size);
        bsp_rs485.answer_stg    = BSP_RS485_ANSWER_WAIT;
        ret                     = BSP_RS485_FUNC_BUSY;
        break;

    case BSP_RS485_ANSWER_WAIT:
        // Ждем выставление флага окончания периода "тишины" после сообщения:
        if (bsp_rs485_sys_tim_flg)  {
            bsp_rs485_sys_tim_flg   = false;
            bsp_rs485.answer_stg    = BSP_RS485_ANSWER_START;
            ret                     = BSP_RS485_FUNC_READY;
            // Переключаем пин DE на прием ==========================:
            GPIO_PDD_ClearPortDataOutputMask(BSP_RS485_PIN_DE_BASE, BSP_RS485_PIN_DE_MASK);
            // Задержка в: 62,5 нс * 4 = 250 мкс ====:
            SYS_TIM_START(4);
            while(bsp_rs485_sys_tim_flg == false)   {};
            bsp_rs485_sys_tim_flg = false;
            // Включаем Rx ========================:
            UART1_TurnRxOn(LDD_Uart1);
        }
    }
    return ret;
}


/************************************
 * @brief   Callback из события по получению очередного байта.
 * @param[in]   None.
 * @return      None
 ************************************/
void bsp_rs485_byte_received_callback (void)
{
    // Запускаем таймер SYS_TIM на 2 мс   или сбрасываем:
	if (bsp_rs485.rx_buff_cnt == 0)	{
	    SYS_TIM_START(MSG_SILENCE_TIME_TICKS);
	}
	else	{
	    SYS_TIM_RESET();
	}
    // Запускаем прием следующего байта:
    bsp_rs485.rx_buff_cnt++;
    UART1_ReceiveBlock(LDD_Uart1, bsp_rs485.rx_buff + bsp_rs485.rx_buff_cnt, 1);
}


/************************************
 * @brief   Callback из события по окончанию отправки сообщения.
 * @param[in]   None.
 * @return      None
 ************************************/
void bsp_rs485_msg_transmit_callback (void)
{
    SYS_TIM_START(MSG_SILENCE_TIME_TICKS);
}

/************************************
 * @brief   Callback из события по обнулению таймера SYS_TIM.
 * @param[in]   None.
 * @return      None
 ************************************/
void bsp_rs485_sys_tim_callback (void)
{
    SYS_TIM_STOP();
    bsp_rs485_sys_tim_flg = true;
}


// --------------------------------------------------------- Private functions -------------------:

// --------------------------------------------------------- Private constants -------------------:

/**********************************************************************************END OF FILE****/
