/**
 * ************************************************************************************************
 * @file    app_main.c
 *
 * @date    05 Feb 2017
 * @author  Ivan / CTMLAB LLC
 * @version 1.2
 * @brief   Main program body
 *
 * @note:   Основной цикл содержит: эхо сервер RS485;
 *          установку значения (out_voltage) на аналоговом выходе ;
 *          получение значения (adc_value) аналогового входа .
 * ************************************************************************************************
 */

// -------------------------------------------------------- Includes -----------------------------:
#include <app_main.h>

#include "Cpu.h"
#include "WDog.h"
#include "bsp/bsp_an_out.h"
#include "bsp/bsp_rs485.h"
#include "bsp/bsp_an_in.h"
#include "bsp/bsp_dig_io.h"

// --------------------------------------------------------- Private defines ---------------------:
// Time defines ===:
#define     MAIN_CYCLE_PERIOD_MS    5       // Заданный в настройках таймера 0 период основного цикла.
#define     ADC_PERIOD_MS           500     // Переодичность запуска АЦП.
#define     AN_OUT_POCESS_PERIOD_MS 500     // Переодичность обновления значения на аналоговом выходе.

/*
 * Размеры буфферов для обмена сообщениями:
 */
#define APP_RX_BUFF_SIZE          200
#define APP_TX_BUFF_SIZE          200

// --------------------------------------------------------- Private types -----------------------:
/*
 * Типы этапов работы интерфейса RS485:
 */
typedef enum {
    APP_MSG_LISTEN, APP_MSG_ANSWER
} app_msg_stg_t;

// --------------------------------------------------------- Private macros ----------------------:

// --------------------------------------------------------- Global variables --------------------:
volatile bool app_timer0_int_flg = false;    // флаг достижения таймером 0 предустановленного значения.

// --------------------------------------------------------- Private variables -------------------:

/*
 * Буфферы для обмена сообщениями по RS485:
 */
static uint8_t rx_msg_buff[APP_RX_BUFF_SIZE];
//static uint8_t tx_msg_buff[APP_TX_BUFF_SIZE];
static uint32_t rx_msg_size;
//static uint32_t tx_msg_size;
static app_msg_stg_t app_msg_stg = APP_MSG_LISTEN;        // Этап работы RS485.

// --------------------------------------------------------- Private function prototypes ---------:

// --------------------------------------------------------- Global functions --------------------:

/***********************
 *		@brief  app_main program body. Executed from main() function.
 *  	@param  None
 *  	@retval None
 **********************/
void app_main(void)
{
    uint16_t adc_timer = 0;    // таймер для периодичности АЦП преобразований.
    uint32_t adc_value = 0;    // результат ацп преобразований в мв.
    uint32_t an_out_process_timer = 0; // таймер для периодичности ЦАП преобразований.
    uint32_t out_voltage = 0; // значение в мв, записываемое в аналоговый выход.

    // Инициализация аналогового выхода  =======:
    bsp_an_out_init();
    bsp_an_out_enable();
    // Инициализация интерфейса rs485  =========:
    bsp_rs485_init();
    // Инициализация дискретных входов/выходов ====:
    bsp_dig_io_init();

    for (;;)   // Period = 5ms
            {
        // =================================== Эхо сервер RS485 :
        if (app_msg_stg == APP_MSG_LISTEN) {
            if (bsp_rs485_listen(rx_msg_buff, &rx_msg_size)
                    == BSP_RS485_FUNC_READY) {
                // call modbus_app();
                app_msg_stg = APP_MSG_ANSWER;
            }
        } else if (app_msg_stg == APP_MSG_ANSWER) {
            if (bsp_rs485_answer(rx_msg_buff, rx_msg_size)
                    == BSP_RS485_FUNC_READY) {
                app_msg_stg = APP_MSG_LISTEN;
            }
        }

        // =================================== Один раз в AN_OUT_POCESS_PERIOD_MS устанавливаем новое значение на аналоговом выходе:
        if (an_out_process_timer++
                >= (AN_OUT_POCESS_PERIOD_MS / MAIN_CYCLE_PERIOD_MS)) {
            an_out_process_timer = 0;
            bsp_an_out_set_voltage(out_voltage); // Устанавливаем новое значение напряжения в мв.
        }

        // ========================================= ADC преобразование  ============================:
        if (adc_timer++ >= (ADC_PERIOD_MS / MAIN_CYCLE_PERIOD_MS)) {
            adc_value = bsp_an_in_value_get();
        }

        // ============= Watch Dog timer clearing (otherwise MCU reset every 200 ms) =================:
        WDog_Clear();

        // ========================================== Ожидание окончания цикла в 5мс ==================:
        while (app_timer0_int_flg != true) {};
        app_timer0_int_flg = false;
    }
}

/**********************************************************************************END OF FILE****/
