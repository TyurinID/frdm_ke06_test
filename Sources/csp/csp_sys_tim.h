/**
 * ************************************************************************************************
 * @file    csp_sys_tim.h
 *
 * @date    3 March 2017
 * @author  Ivan / CTMLAB LLC
 * @version 1.0
 * @brief   Блок содержит макросы для работы с SYS_TIM таймером (входит в ядро Cortex-M).
 * @note    Таймер считает в системных тиках.
 *          Для успешной работы прерываний следует подключить в Processor Expert модуль
 *          TimerUnit_LDD, выбрать SysTick и включить прерывания. Прочие функции можно выключить.
 * ************************************************************************************************
 */
#ifndef CSP_CSP_SYS_TIM_H_
#define CSP_CSP_SYS_TIM_H_

// -------------------------------------------------------- Includes -----------------------------:
#include <stdint.h>
#include <stdbool.h>
#include "MKE06Z4.h"
// -------------------------------------------------------- Exported define ----------------------:

// -------------------------------------------------------- Exported types -----------------------:
// -------------------------------------------------------- Private macros -----------------------:

/*
 * @brief   Макрос включает подсчет таймера SYS_TIM. Через (time_ticks) в системных тиках
 *          генерируется прерывание.
 * @param[in]   time_ticks = [1..16777215], при 16 MHz : 1 тик = 62,5 нс.
 */
#define SYS_TIM_START(time_ticks)  if(true)   {     \
        SYST_RVR = SysTick_RVR_RELOAD(time_ticks);  \
        SYST_CVR = SysTick_CVR_CURRENT(0x00);       \
        SYST_CSR = (SysTick_CSR_ENABLE_MASK | SysTick_CSR_CLKSOURCE_MASK | SysTick_CSR_TICKINT_MASK); \
    }

/*
 * @brief   Макрос выключает подсчет таймера SYS_TIM.
 */
#define SYS_TIM_STOP()  if(true)    {           \
        SYST_CSR &= (~SysTick_CSR_ENABLE_MASK); \
    }

/*
 * @brief   Макрос сбрасывает текущее значение таймера SYS_TIM.
 */
#define SYS_TIM_RESET()  if(true)    {        \
        SYST_CVR = SysTick_CVR_CURRENT(0xff); \
    }

/*
 * @brief   Макрос возвращает количество прошедших тиков с момента запуска SYS_TIM.
 * @note    Должен быть вызыван до обнуления таймера.
 */
#define SYS_TIM_GET_TICKS()  (uint32_t)(((0xffffff) & (SYST_RVR)) - ((0xffffff) & (SYST_CVR)))
// -------------------------------------------------------- Exported variables -------------------:
// -------------------------------------------------------- Exported functions -------------------:
// -------------------------------------------------------- Exported constants -------------------:

#endif /* CSP_CSP_SYS_TIM_H_ */
